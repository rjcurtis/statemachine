﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test.Events
{
    public class CashInEvent : StateMachine.IStateEvent
    {
        public long CreditsIn { get; set; }
    }
}
