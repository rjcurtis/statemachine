﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test
{
    public class ConsoleKeyEvent : StateMachine.IStateEvent
    {
        public ConsoleKeyInfo KeyInfo { get; set; }
    }
}
