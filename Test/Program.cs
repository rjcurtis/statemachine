﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Test.Events;
using Test.States;

namespace Test
{
    class Program
    {
        static void Main(string[] args)
        {
            var machine = new StateMachine.Machine();
            var idle = new Idle();
            var idleWithCredits = new IdleWithCredits();
            idleWithCredits.Parent = idle;
            var idleWithoutCredits = new IdleWithoutCredits();
            idleWithoutCredits.Parent = idle;
            var playGame = new PlayGame();

            machine.AddState(idle);
            machine.AddState(idleWithCredits);
            machine.AddState(idleWithoutCredits);
            machine.AddState(playGame);

            machine.InitialState(idle);

            ConsoleKeyInfo keyinfo;
            while (true)
            {
                if(Console.KeyAvailable)
                {
                    keyinfo = Console.ReadKey();

                    if (keyinfo.Key == ConsoleKey.Add)
                    {
                        machine.PushEvent(new CashInEvent { CreditsIn = 100 });
                    }
                    if (keyinfo.Key == ConsoleKey.Subtract)
                    {
                        machine.PushEvent(new CashOutEvent());
                    }
                    if (keyinfo.Key == ConsoleKey.Enter)
                    {
                        machine.PushEvent(new PlayButtonPushedEvent { WasMechanicalButton = false });
                    }
                    if (keyinfo.Key == ConsoleKey.Escape)
                        return;
                }

                machine.Update();
            }
        }
    }
}
