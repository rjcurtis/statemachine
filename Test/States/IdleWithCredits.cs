﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StateMachine;
using Test.Events;

namespace Test
{
    public class IdleWithCredits : StateMachine.State
    {
        public IdleWithCredits()
        {
            EventsSubscribed.Add(typeof(CashOutEvent), HandleCashOut);
        }

        private bool HandleCashOut(IStateEvent evnt)
        {
            var cashoutEvent = evnt as CashOutEvent;
            PlayerSingleton.Credits = 0;
            TransitionEvent(typeof (Idle));
            return true;
        }

        public override void Enter()
        {
            
        }

        public override void Exit()
        {

        }
    }
}
