﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StateMachine;
using Test.Events;
using Test.States;

namespace Test
{
    public class Idle : State
    {
        public Idle()
        {
            EventsSubscribed.Add(typeof(CashInEvent), HandleCashIn);
        }

        private bool HandleCashIn(IStateEvent evnt)
        {
            var cashInEvent = evnt as CashInEvent;
            PlayerSingleton.Credits += cashInEvent.CreditsIn;

            if (PlayerSingleton.Credits > 0)
                Transition(typeof(IdleWithCredits));
            else
                Transition(typeof(IdleWithoutCredits));

            return true;
        }

        public override void Enter()
        {
            if(PlayerSingleton.Credits > 0)
                Transition(typeof(IdleWithCredits));
            else
                Transition(typeof(IdleWithoutCredits));
        }

        public override void Exit()
        {
            
        }
    }
}
