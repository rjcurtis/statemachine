﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StateMachine
{
    public class Machine
    {
        private List<State> _states = new List<State>();
        private ConcurrentQueue<IStateEvent> _events = new ConcurrentQueue<IStateEvent>();
        private Queue<State> _waitingTransitions = new Queue<State>();  
        private State _currentState;

        public bool VerboseLogging { get; set; }

        public Machine()
        {
            
        }

        public void Update()
        {
            PerformTransitions();
            PumpEvents();
        }

        private void PerformTransitions()
        {
            if (_waitingTransitions.Count == 0)
                return;

            var foundTransition = _waitingTransitions.Dequeue();
            //_currentState = foundTransition;

            NotifyAncestorsOfEnterIfExist(foundTransition);

            //Console.WriteLine("Entering {0}", _currentState);
            //_currentState.Enter();
        }

        private void NotifyAncestorsOfEnterIfExist(State destinationState)
        {
            State lowestAncestor = null;

            var ancestor = _currentState.Parent;
            while (ancestor != null)
            {
                var destinationAncestor = destinationState.Parent;
                while (destinationAncestor != null)
                {
                    if (destinationAncestor == ancestor)
                        lowestAncestor = destinationAncestor;
                    destinationAncestor = destinationAncestor.Parent;
                }
                ancestor = ancestor.Parent;
            }

            _currentState = destinationState;

            if (lowestAncestor != null)
            {
                var graph = new List<State>();
                graph.Add(destinationState);
                ancestor = destinationState.Parent;
                if (ancestor != lowestAncestor)
                {
                    graph.Add(ancestor);
                    ancestor = ancestor.Parent;
                }

                graph.Reverse();
                graph.ForEach(state =>
                {
                    Console.WriteLine("Entering state: {0}", state);
                    state.Enter();
                });
            }
            
        }

        public void InitialState(State state)
        {
            if(!_states.Contains(state))
                throw new Exception(string.Format("Initial state '{0}' not found", state));

            _currentState = state;
            _currentState.Enter();
        }

        public void InitialState(Type type)
        {
            var found = _states.FirstOrDefault(p => p.GetType() == type);
            if (found == null)
            {
                throw new Exception(string.Format("Initial state unknowne: {0}", type));
            }

            _waitingTransitions.Enqueue(found);
        }

        public void AddState(State state)
        {
            if(_states.Contains(state))
                throw new Exception("Duplicate states not allowed");

            state.TransitionEvent += HandleTransition;
            _states.Add(state);
        }

        private void HandleTransition(Type newState)
        {
            var stateFound = _states.FirstOrDefault(state => state.GetType() == newState);
            if(stateFound == null)
                throw new Exception(string.Format("State transition requested to unknown state '{0}'", newState));

            Console.WriteLine("Exiting {0}", _currentState);
            //_currentState.Exit();
            NotifyAncestorsOfExitIfExist(stateFound);
            _waitingTransitions.Enqueue(stateFound);
        }

        private void NotifyAncestorsOfExitIfExist(State destinationState)
        {
            State lowestAncestor = null;

            var ancestor = _currentState.Parent;
            while (ancestor != null)
            {
                var destinationAncestor = destinationState.Parent;
                while (destinationAncestor != null)
                {
                    if (destinationAncestor == ancestor)
                        lowestAncestor = destinationAncestor;
                    destinationAncestor = destinationAncestor.Parent;
                }
                ancestor = ancestor.Parent;
            }

            if (lowestAncestor != null)
            {
                Console.WriteLine("Found lowest common ancestory: {0}", lowestAncestor);

                var graph = new List<State>();
                graph.Add(_currentState);
                var nextAncestor = _currentState.Parent;
                while (nextAncestor != null)
                {
                    graph.Add(nextAncestor);
                    nextAncestor = nextAncestor.Parent;
                }

                graph.Remove(graph.Last());

                graph.ForEach(state =>
                {
                    Console.WriteLine("Exiting {0}", state);
                    state.Exit();
                });
            }
        }

        /// <summary>
        /// Could be called from other threads, don't pump events from here.
        /// </summary>
        public void PushEvent(IStateEvent evnt)
        {
            _events.Enqueue(evnt);
            //Console.WriteLine("Event {0} enqeued.");
        }

        private void PumpEvents()
        {
            if (_events.IsEmpty)
                return;

            IStateEvent evnt;
            var success = _events.TryDequeue(out evnt);
            if (!success)
                throw new Exception("Failed to process Event in state machine!");

            //Console.WriteLine("Pushing event {0}", evnt);

            if (_currentState.EventsSubscribed.ContainsKey(evnt.GetType()))
            {
                var handler = _currentState.EventsSubscribed[evnt.GetType()];
                var handled = handler(evnt);
                if (handled)
                {
                    //Console.WriteLine("Event handled by {0}", handler);
                    return;
                }
            }

            // Search up the parent graph
            var parent = _currentState.Parent;
            while (parent != null)
            {
                if (parent.EventsSubscribed.ContainsKey(evnt.GetType()))
                {
                    var handler = parent.EventsSubscribed[evnt.GetType()];
                    var handled = handler(evnt);
                    if (handled)
                    {
                        //Console.WriteLine("Event handled by {0}", handler);
                        return;
                    }
                }
                parent = parent.Parent;
            }
            
        }
    }
}
