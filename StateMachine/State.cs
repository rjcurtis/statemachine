﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StateMachine
{
    public abstract class State
    {
        public delegate bool EventHandler(IStateEvent evnt);

        public delegate void TransitionEventHandler(Type newState);

        public TransitionEventHandler TransitionEvent { get; set; }

        private List<State> _childStates = new List<State>();

        public Dictionary<Type, EventHandler> EventsSubscribed = new Dictionary<Type, EventHandler>();

        public State Parent { get; set; }

        public abstract void Enter();

        public abstract void Exit();

        protected void Transition(Type state)
        {
            var handler = TransitionEvent;
            if (handler != null)
                handler(state);
        }

        //protected abstract bool HandleEvent(IStateEvent evnt);

        public State()
        {
            Parent = null;
        }

        public void AddChildState(State state)
        {
            if(_childStates.Contains(state))
                throw new Exception("Duplicate child states not allowed");

            state.Parent = this;
            _childStates.Add(state);
        }
    }
}
