﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransitionTest.Events;
using TransitionTest.States;

namespace TransitionTest
{
    class Program
    {
        static void Main(string[] args)
        {
            var stateMachine = new StateMachine.Machine();

            var a = new A();  // root graph node

            var b = new B { Parent = a };
            var c = new C { Parent = b };

            var d = new D { Parent = a };
            var e = new E { Parent = d };

            stateMachine.AddState(a);
            stateMachine.AddState(b);
            stateMachine.AddState(c);
            stateMachine.AddState(d);
            stateMachine.AddState(e);

            stateMachine.InitialState(c);

            ConsoleKeyInfo keyinfo;
            while (true)
            {
                if (Console.KeyAvailable)
                {
                    keyinfo = Console.ReadKey();

                    if (keyinfo.Key == ConsoleKey.Enter)
                    {
                        stateMachine.PushEvent(new GoGoGo());
                    }

                    if (keyinfo.Key == ConsoleKey.Escape)
                        return;
                }

                stateMachine.Update();
            }

        }
    }
}
