﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StateMachine;
using TransitionTest.Events;

namespace TransitionTest.States
{
    public class E : StateMachine.State
    {
        public E()
        {
            EventsSubscribed.Add(typeof(GoGoGo), HandleGoEvent);
        }

        private bool HandleGoEvent(IStateEvent evnt)
        {
            Transition(typeof(C));
            return true;
        }

        public override void Enter()
        {
            
        }

        public override void Exit()
        {
        }
    }
}
